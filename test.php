<?php
$n = 5; // specify how many rows you want to
$stars = 0;

for ($i = $n; $i > 0; $i--) {
    for ($j = 0; $j < $i + $stars; $j++) {
        if ($j < $i - 1) {
            echo "\n ";
        } else {
            echo "*";
        }
    }

    $stars += 2;
    echo "\n";
}

?>