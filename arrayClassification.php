<?php
//multidimentional array
$student = array(
	"name"=> "ROVA",
	"roll"=> "G-034039",
	"age"=> "24",
	"address" => array(
		"vill" => "Raozan",
		 "upazila" => "Raozan",
		"district" => "Chittagong",
		"home" => array("place" => "ctg")     
						),
	"education level"=> "Graduation",
					);

print_r($student);
echo "<br><br><br>";
echo $student['name']."<br>";
echo $student['address']['vill']."<br>";
echo $student['address']['home']['place']."<br>";