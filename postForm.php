<!DOCTYPE html>
<html>
<head>
	<title>Form validation with POST method</title>

	<style type="text/css">
	h3{font-family: Calibri; font-size: 22pt; font-style: normal; font-weight: bold; color:SlateBlue;
	text-align: center; text-decoration: underline }
	table{font-family: Calibri; color:black; font-size: 11pt; font-style: normal;
	text-align:; background-color: #CCFFCC; border-collapse: collapse; border: 2px solid navy}
	table.inner{border: 0px}
	</style>
</head>
<body>
 
 <h3>STUDENT REGISTRATION FORM</h3>
	<form action="form.php" method="POST" enctype="multipart/form-data">
	 <!------enctype="multipart/form-data" for file uploading ---=-->
	<table align="center" cellpadding = "10">
		<tr>
		<td>FIRST NAME</td>
		<td><input type="text" name="First_Name"  placeholder= "FirstName" maxlength="30"/>
		(max 30 characters a-z and A-Z)
		</td>
		</tr>
		 
		<!----- Last Name ---------------------------------------------------------->
		<tr>
		<td>LAST NAME</td>
		<td><input type="text" name="Last_Name" placeholder= "LastName" maxlength="30"/>
		(max 30 characters a-z and A-Z)
		</td>
		</tr>
		 
		<!----- Date Of Birth -------------------------------------------------------->
		<tr>
		<td>DATE OF BIRTH</td>
		 
		<td>
		<select name="Birthday_day" id="Birthday_Day">
		<option value="-1">Day:</option>
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		 
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>
		<option value="7">7</option>
		<option value="8">8</option>
		<option value="9">9</option>
		<option value="10">10</option>
		<option value="11">11</option>
		<option value="12">12</option>
		 
		<option value="13">13</option>
		<option value="14">14</option>
		<option value="15">15</option>
		<option value="16">16</option>
		<option value="17">17</option>
		<option value="18">18</option>
		<option value="19">19</option>
		<option value="20">20</option>
		<option value="21">21</option>
		 
		<option value="22">22</option>
		<option value="23">23</option>
		<option value="24">24</option>
		<option value="25">25</option>
		<option value="26">26</option>
		<option value="27">27</option>
		<option value="28">28</option>
		<option value="29">29</option>
		<option value="30">30</option>
		 
		<option value="31">31</option>
		</select>
		 
		<select id="Birthday_Month" name="Birthday_Month">
		<option value="">Month:</option>
		<option value="January">Jan</option>
		<option value="February">Feb</option>
		<option value="March">Mar</option>
		<option value="April">Apr</option>
		<option value="May">May</option>
		<option value="June">Jun</option>
		<option value="July">Jul</option>
		<option value="August">Aug</option>
		<option value="September">Sep</option>
		<option value="October">Oct</option>
		<option value="November">Nov</option>
		<option value="December">Dec</option>
		</select>
		 
		<select name="Birthday_Year" id="Birthday_Year">
		 
		<option value="-1">Year:</option>
		<option value="2017">2017</option>
		<option value="2016">2016</option>
		<option value="2015">2015</option>
		<option value="2014">2014</option>
		<option value="2013">2013</option>
		<option value="2012">2012</option>
		<option value="2011">2011</option>
		<option value="2010">2010</option>
		<option value="2009">2009</option>
		<option value="2008">2008</option>
		<option value="2007">2007</option>
		<option value="2006">2006</option>
		<option value="2005">2005</option>
		<option value="2004">2004</option>
		<option value="2003">2003</option>
		<option value="2002">2002</option>
		<option value="2001">2001</option>
		<option value="2000">2000</option>
		 
		<option value="1999">1999</option>
		<option value="1998">1998</option>
		<option value="1997">1997</option>
		<option value="1996">1996</option>
		<option value="1995">1995</option>
		<option value="1994">1994</option>
		<option value="1993">1993</option>
		<option value="1992">1992</option>
		<option value="1991">1991</option>
		<option value="1990">1990</option>
		 
		</select>
		</td>
		</tr>
		 
		<!----- Email Id ---------------------------------------------------------->
		<tr>
		<td>EMAIL ID</td>
		<td><input type="email" name="Email_Id" placeholder= "Example@gmail.com"  maxlength="100" /></td>
		</tr>
		 
		<!----- Mobile Number ---------------------------------------------------->
		<tr>
		<td>MOBILE NUMBER</td>
		<td>
		<input type="number" name="Mobile_Number" maxlength="10" />
		(10 digit number)
		</td>
		</tr>
		 
		<!----- Gender ---------------------------------------------------->
		<tr>
		<td>GENDER</td>
		<td>
		Male <input type="radio" name="Gender" value="Male" />
		Female <input type="radio" name="Gender" value="Female" />
		</td>
		</tr>
		 
		<!----- Address ---------------------------------------------------->
		<tr>
		<td>ADDRESS <br><br><br></td>
		<td><textarea name="Address" rows="4" cols="30"></textarea></td>
		</tr>


		<!----- Submit and Reset ------------------------------------------>
		<tr>
		<td colspan="2" align="center">
		<input type="submit" value="Submit">
		</td>
		<td>
		<input type="reset" value="Reset">
		</td>
		</tr>
   	</table>

   </form>
</body>
</html>
<?php
